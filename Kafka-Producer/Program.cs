﻿// See https://aka.ms/new-console-template for more information

using System.Text.Json;
using System.Text.Json.Serialization;
using Confluent.Kafka;

Console.WriteLine("Hello, World!");

var producerConfig = new ProducerConfig
{
    BootstrapServers = "PLAINTEXT://kafka-0:9092"
};

using var producer = new ProducerBuilder<Null, JobMessage>(producerConfig)
    .SetValueSerializer(new JobMessageSerializer())
    .Build();

var jm = new JobMessage {Id = Guid.NewGuid(), Url = "https://heise.de", Width = 1980, Height = 1090};

//var json = JsonSerializer.Serialize(jm);
//Console.WriteLine($"Create message: {json}");
//await producer.ProduceAsync("jobrequests", new Message<Null, JobMessage>{Value = jm});
await foreach (var deliveryResult in ConsumerDing(producer)) 
    Console.WriteLine(JsonSerializer.Serialize(deliveryResult.Message.Value));

static async IAsyncEnumerable<DeliveryResult<Null, JobMessage>> ConsumerDing(IProducer<Null, JobMessage> producer)
{
    for (var i = 0; i <= 1; i++)
    {
        yield return await producer.ProduceAsync("jobrequests",
            new Message<Null, JobMessage>
            {
                Value = new JobMessage
                    {Id = Guid.NewGuid(), Height = 1080, Width = 1920, Url = $"https://heise.de/{i}"}
            });
    }
}


public class JobMessage
{
    [JsonPropertyName("id")] public Guid Id { get; set; }

    [JsonPropertyName("url")] public string? Url { get; set; }

    [JsonPropertyName("width")] public int Width { get; set; }

    [JsonPropertyName("height")] public int Height { get; set; }
}

public class JobMessageSerializer : ISerializer<JobMessage>
{
    public byte[] Serialize(JobMessage data, SerializationContext context)
    {
        var json = JsonSerializer.Serialize(data);
        return System.Text.Encoding.UTF8.GetBytes(json);
    }
}